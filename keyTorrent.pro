#-------------------------------------------------
#
# Project created by QtCreator 2017-07-31T14:27:05
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = keyTorrent
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES +=\
    bencoder/bencoder.cpp \
    bencoder/bitem.cpp \
    bencoder/blist.cpp \
    bencoder/bdict.cpp \
    bencoder/testbencoder.cpp \
    main.cpp \
    mainwindow.cpp \
    httpclient/httpclient.cpp \
    pwpclient/pwpclient.cpp

HEADERS += \
    bencoder/bencoder.h \
    bencoder/bitem.h \
    bencoder/blist.h \
    bencoder/bdict.h \
    bencoder/testbencoder.h \
    mainwindow.h \
    httpclient/httpclient.h \
    pwpclient/pwpclient.h

FORMS += \
        mainwindow.ui
