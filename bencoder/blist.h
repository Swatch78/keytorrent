#ifndef BLIST_H
#define BLIST_H

#include <vector>
#include <mutex>
#include "bitem.h"

class BList
{
public:
    BList();
    void addItem(BItem* item);
    BItem* operator[](int index);
private:
    std::mutex mut;
    std::vector<BItem*> itemsList;
};

#endif // BLIST_H
