#ifndef BITEM_H
#define BITEM_H
#include <string>

class BItem
{
private:
    bool isDigit;
    int intValue;
    std::string strValue;
public:
    BItem();
    BItem(int value);
    BItem(const std::string& value);
    BItem( const BItem& other ) = delete;
    BItem& operator=( const BItem& other ) = delete;
#if defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
    BItem( BItem&& other );
    BItem& operator=(BItem&& other ) noexcept;
#endif
    int getIntValue() const;
    void setIntValue(int value);
    std::string getStrValue() const;
    void setStrValue(const std::string &value);


};

#endif // BITEM_H
