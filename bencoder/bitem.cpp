#include "bitem.h"

BItem::BItem()
{
}

BItem::BItem(int value): isDigit(true), intValue(value), strValue(nullptr)
{
}

BItem::BItem(const std::string& value): isDigit(false), intValue(0), strValue(value)
{
}

int BItem::getIntValue() const
{
    return intValue;
}

void BItem::setIntValue(int value)
{
    intValue = value;
}

std::string BItem::getStrValue() const
{
    return strValue;
}

void BItem::setStrValue(const std::string &value)
{
    strValue = value;
}
