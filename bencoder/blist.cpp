#include "blist.h"

BList::BList()
{

}

void BList::addItem(BItem *item)
{
    std::lock_guard<std::mutex> lock(mut);
    itemsList.push_back(item);
}

BItem* BList::operator[](int index)
{
    return itemsList.at(index);
}
