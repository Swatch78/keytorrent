#ifndef BDICT_H
#define BDICT_H

#include <map>
#include "bitem.h"

class BDict
{
public:
    BDict();
private:
    std::map<BItem,BItem> itemsmap;
};

#endif // BDICT_H
